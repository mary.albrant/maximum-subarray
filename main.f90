program MaxMatrix
 use :: mpi
 use :: Task
 implicit none
 real(8), allocatable, dimension(:,:) :: A
 integer(4) :: x1, x2, y1, y2
 integer(4) :: i, m, n, mpiRank, mpiErr
 
 open(1, file = "input1")
 read(1,*) m, n
 allocate (A(m,n))
 do i = 1, m
   read(1,*) A(i,:)
 enddo
  
 call mpi_init(mpiErr) 
 call mpi_comm_rank(MPI_COMM_WORLD, mpiRank, mpiErr)
 
 call GetMaxCoordinates(A, x1, y1, x2, y2)  
 
 if ( mpiRank == 0 ) then
   write(*,*) "indexes :", x1, y1, x2, y2
 endif
 call mpi_finalize(mpiErr)
 close(1)

end program
